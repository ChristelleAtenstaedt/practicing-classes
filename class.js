const { isWhiteSpaceLike } = require("typescript");

class Animal {
  constructor(name, breed) {
    this.name = name;
    this.breed = breed;
  }

  speak() {
    console.log(`${this.name} makes a noise`);
  }
}

class Dog extends Animal {
  constructor(name, breed, color) {
    super(name, breed);
    this.color = color;
  }
  speak() {
    console.log(`${this.name} barks`);
  }
  showDog() {
    console.log(
      `${this.name}, a dog of the ${this.breed} breed, has won the gold medal!`
    );
  }
}

const d = new Dog("Ralph", "Terrier", "black");
d.speak();
d.showDog();

class Cat {
  constructor(name) {
    this.name = name;
  }

  speak() {
    console.log(`${this.name} makes a noise.`);
  }
}

class Lion extends Cat {
  speak() {
    super.speak();
    console.log(`${this.name} roars.`);
  }
}

const l = new Lion("Fuzzy");
l.speak();

var obj = {};

console.log(obj.__proto__);

// This code defines a constructor function called Account. Constructor functions in JavaScript are used to create objects with a specific structure and behavior. This constructor function allows you to create multiple Account objects, each with its own balance and ability to deposit money.
// balance: This is a property of the Account object, representing the current balance.
// deposit: This is a method (function) of the Account object, used to deposit money into the account. It takes one parameter, amount, which represents the amount of money to deposit. When called, it increases the balance property by the specified amount.

function Account(balance) {
  this.balance = balance;

  this.deposit = function (amount) {
    this.balance += amount;
  };

  // Internal property that you can attatch properties and methods to shared across all object that you create:
  Account.prototype =  {
    deposit: function(amount) {
      this._balance += amount;
  },
get balance() {
  return this._balance
},
set balance(balance) {
if (balance < 0)
throw 'Balance cannot be negative'
this._balance = balance
}
  }

// Create a new Account object with an initial balance of 1000

var acct = new Account(1000);

console.log(acct.balance);

acct.deposit(200);

console.log(acct.balance);

var acct2 = new Account(550);
console.log(acct2.balance);

acct2.deposit(100);

console.log(acct2.balance);

// Instead of the above, using ES6 classes, which provide a more modern and concise syntax for defining constructor functions and methods.
class Accounts {
  constructor(balance) {
    this.balance = balance;
  }
  deposit(amount) {
    this.balance += amount;
  }
}

let acc = new Accounts(700);
console.log(acc.balance);

acc.deposit(100);
console.log(acc.balance);
console.log("deposit" in Accounts.prototype); 

acc.balance = "hdjshjsh";
console.log(acc.balance);


